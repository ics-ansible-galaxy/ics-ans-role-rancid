ics-ans-role-rancid
===================

Ansible role to install rancid.

Requirements
------------

- ansible >= 2.7
- molecule >= 2.19

Role Variables
--------------

```yaml
...
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-rancid
```

License
-------

BSD 2-clause
